<?php

namespace configs;

/**
 * Класс, содержащий в себе конфигурационные параметры приложения
 */
class Config
{
    /**
     * Драйвер, использующийся для работы с записями
     *
     * @var string
     */
    public static $driver_name;

    /**
     * Необходимые параметры для работы драйвера
     *
     * @var array
     */
    public static $driver_params = [];

    /**
     * Домашняя страница URL
     *
     * @var string
     */
    public static $default_url = '/delivery';

    /**
     * Путь до папки с шаблонами
     * @var string
     */
    public static $path_to_templates = '';
}

/**
 * задаем тип хранилища
 */
Config::$driver_name = 'ArrayDriver';

/**
 * задаем специальные параметры для хранилища
 */
Config::$driver_params['ArrayDriver'] = [
    //Товары для доставки
    'goods' => [
        ['id' => 1, 'name' => 'Утюг 1', 'code' => '1001'],
        ['id' => 11, 'name' => 'Утюг 1', 'code' => '10011'],
        ['id' => 2, 'name' => 'Утюг 2', 'code' => '1002'],
        ['id' => 3, 'name' => 'Утюг 3', 'code' => '1003'],
        ['id' => 4, 'name' => 'Утюг 4', 'code' => '1004'],
        ['id' => 5, 'name' => 'Утюг 5', 'code' => '1005'],
        ['id' => 6, 'name' => 'Утюг 6', 'code' => '1006'],
        ['id' => 7, 'name' => 'Утюг 7', 'code' => '1007'],
        ['id' => 8, 'name' => 'Утюг 8', 'code' => '1008']
    ],
    //Службы доставки
    'service' => [
        [
            'id'         => 1,
            'name'       => 'Птичка',
            'className'  => 'bird',
            'config'     => '{"admission_time": {"hour": "18", "min": "00"}}' // JSON строка, храни дополнительные парамтеры "Время после которого заявки не принимаются"
        ],
        [
            'id'         => 2,
            'name'       => 'Черепаха',
            'className'  => 'tortoise',
            'config'     => '{"base_cost": 150}' // JSON строка, храни дополнительные парамтеры "Базовая стоимость"
        ]
    ]
];

Config::$path_to_templates = dirname(__DIR__) . '/templates/';
