<?php

namespace components;

use models\Goods;

/**
 * Базовый класс для взаимодействия с API службы доставки
 */
abstract class SimpleService
{
    protected $kladr, $goods, $config;

    /**
     * Конструктор класса
     *
     * @param string $kladr
     * @param Goods $goods
     * @param array $config
     */
    public function __construct($kladr, Goods $goods, array $config)
    {
        $this->kladr = $kladr;
        $this->goods = $goods;
        $this->config = $config;
    }

    /**
     * Метод возвращает дату и стоимость доставки
     *
     * @return array
     */
    abstract public function getData();
}
