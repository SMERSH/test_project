<?php

namespace components;

/**
 * Класс для взаимодействия с API службы доставки "Птичка"
 */
class BirdService extends SimpleService
{
    public function __construct($kladr, $goods,$config)
    {
        parent::__construct($kladr, $goods, $config);
    }

    /**
     * Метод, возвращает данные о доставке(стоимость и дату)
     *
     * @return array
     */
    public function getData()
    {
        $time = time();
        $date_time_array = getdate($time);
        $timestamp = mktime(
            $this->config['admission_time']['hour'],
            $this->config['admission_time']['min'],
            0,
            $date_time_array['mon'],
            $date_time_array['mday'],
            $date_time_array['year']
        );

        // Если текущее время больше указанного в конфиге, то заявки не принимаются
        if ($time > $timestamp)
            return ['error' => 'Заявки после ' . $this->config['admission_time']['hour'] . ':' .
                $this->config['admission_time']['min'] . ' не принимаются'
            ];

        // обращание к API службы доставки
        $result = $this->api($this->kladr, $this->goods->getCode());

        //Проверяем была ли ошибка
        if(!empty($result['error'])){
            return ['error' => $result['error']];
        }

        return [
            'price'  => $result['price'],
            'date'   => strtotime('+' . --$result['period'] . 'day'),
            'error'  => $result['error']
        ];
    }

    /**
     * Метод для взаимодествия с API службы сдоставки
     *
     * @param $kladr
     * @param $code
     * @return array
     */
    protected function api($kladr, $code)
    {
        return [
            'price'  => number_format(rand(100.00, 15000.00), 2, '.', ' '), // Генерируем стоимость доставки
            'period' => rand(2, 14), // Генерируем количество дней для доставки
            'error'  => ''
        ];
    }
}
