<?php

namespace components;

/**
 * Класс для взаимодействия с API службы доставки "Черепашка"
 */
class TortoiseService extends SimpleService
{
    public function __construct($kladr, $goods, $config)
    {
        parent::__construct($kladr, $goods, $config);
    }

    /**
     * Метод, возвращает данные о доставке(стоимость и дату)
     *
     * @return array
     */
    public function getData()
    {
        // обращание к API службы доставки
        $result = $this->api($this->kladr, $this->goods->getName());

        //Проверяем была ли ошибка
        if(!empty($result['error'])){
            return ['error' => $result['error']];
        }

        return [
            'price'  => number_format($this->config['base_cost'] * $result['coefficient'], 2, '.', ' '), // Перемножаем базовую стоимость с коэффициентом
            'date'   => $result['date'],
            'error'  => $result['error']
        ];
    }


    /**
     * Метод для взаимодествия с API службы сдоставки
     *
     * @param $kladr
     * @param $name
     * @return array
     */
    protected function api($kladr, $name)
    {
        return [
            'coefficient' => rand(100, 999)/100, // Генерируем коэффициент
            'date'        => strtotime('+' . rand(1, 14). 'day'), // Генерируем дату доставки timestamp
            'error'       => ''
        ];
    }
}
