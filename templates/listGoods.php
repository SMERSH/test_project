<div class="title">
    <h1>Список товаров для доставки</h1>
</div>
<form class="form-inline search" method="post">
    <div class="form-group">
        <label>Поиск</label>
        <label for="name" class="sr-only">по наименованию</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="по наименованию" value="<?= $params['name'] ?>">
    </div>
    <div class="form-group">
        <label for="code" class="sr-only">по коду</label>
        <input type="text" class="form-control" id="code" name="code" placeholder="по коду" value="<?= $params['code'] ?>">
    </div>
    <button type="submit" class="btn btn-primary">Найти</button>
</form>
<table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Товар</th>
            <th>Код</th>
            <th>Доставка</th>
        </tr>
    </thead>
    <tbody>

    <?php if (count($goods) > 0): ?>
        <?php foreach($goods as $key => $item): ?>
            <tr>
                <td class="text-center"><?= ++$key ?></td>
                <td><?= $item->getName() ?></td>
                <td><?= $item->getCode() ?></td>
                <td>
                    <button class="btn btn-info show-modal" data-toggle="modal" data-target="#info-modal" id="<?= $item->getId() ?>">Узнать стоимость и дату доставки</button>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else: ?>
        <tr><td colspan="4">Список товаров пуст</td></tr>
    <?php endif ?>
    </tbody>
</table>

<div class="modal fade" id="info-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Стоимость и дата доставки</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="modal-form">
                    <input class="form-control" id="goods_id" name="goods_id" style="display: none">
                    <div class="form-group">
                        <label for="kladr" class="col-sm-4 control-label">Город</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="kladr" name="kladr">
                                <option value="0">Выберите город</option>
                                <option value="1">Амстердам</option>
                                <option value="2">Кострома</option>
                                <option value="3">Мадрид</option>
                                <option value="4">Москва</option>
                                <option value="5">Осло</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="service_id" class="col-sm-4 control-label">Служба доставки</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="service_id" name="service_id">
                                <option value="0">Выберите службу доставки</option>
                                <?php foreach($services as $service): ?>
                                    <option value="<?= $service->getId() ?>"><?= $service->getName() ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-sm-4 control-label">Стоимость</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="price"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date" class="col-sm-4 control-label">Дата доставки</label>
                        <div class="col-sm-8">
                            <p class="form-control-static" id="date"></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-success" id="submit">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $('.show-modal').click(function() {
        clearForm();
        $('#goods_id').val($(this).attr('id'));
    });

    $("#kladr, #service_id").on('change',function(){
        $('.error').fadeOut();
        $('.error').remove();
        if ($('select#kladr').val() != 0 && $('select#service_id').val() != 0){
            $.ajax({
                url: '/delivery/getDeliveryData',
                type: 'post',
                data: $('form#modal-form').serialize(),
                dataType: 'json',
                success: function(data) {
                    $('#submit').prop('disabled', false);
                    if (data.error) {
                        $('form#modal-form').after('<div class="alert alert-danger error" style="display: none">' + data.error + '</div>');
                        $('.error').fadeIn();
                        $('#submit').prop('disabled', true);
                    } else {
                        $('p#price').text(formatterAsRub(data.price));
                        $('p#date').text(moment(data.date * 1000).format('DD.MM.YYYY'));
                    }
                },
                error: function (jqXHR, exception) {
                    console.log(jqXHR, exception);
                }
            });
        }
    });

    $("button#submit").click(function(){
        if($('select#kladr').val() != 0 && $('select#service_id').val() != 0){
            $('button#' + $('#goods_id').val()).parent().parent().addClass('success');
            var text = '<div class="col-lg-6"><ul>' +
                '<li>Служба доставки: ' + $('#service_id option:selected').html() + '</li>'+
                '<li>Адрес: ' + $('#kladr option:selected').html() + '</li>'+
                '</ul></div>'+
                '<div class="col-lg-6"><ul>' +
                '<li>Стоимость: ' + $('#price').text() + '</li>'+
                '<li>Дата доставки: ' + $('#date').text() + '</li>'+
                '</ul></div>'
            $('button#' + $('#goods_id').val()).parent().html(text);

            clearForm();
            $('#info-modal').modal('hide');
        }else{
            showError('Заполните все поля формы.');
        }
    });

    function clearForm() {
        $('form#modal-form').trigger( 'reset' );
        $('#price').text('');
        $('#date').text('');
        $('.error').remove();
    }

    function showError(text) {
        jQuery('form#modal-form').after('<div class="alert alert-danger error" style="display: none">' + text + '</div>');
        jQuery('.error').fadeIn();
    }

    function formatterAsRub(value) {
        let parts = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        return parts.join(",") + ' руб.';
    }
});
</script>