<?php
use widgets\Menu\MenuWidget;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Тестовый проект</title>

    <link href="/templates/css/bootstrap.min.css" rel="stylesheet">
    <link href="/templates/css/style.css" rel="stylesheet">
    <script src="/templates/js/jquery-3.2.1.min.js"></script>
    <script src="/templates/js/bootstrap.min.js"></script>
    <script src="/templates/js/moment.js"></script>
</head>

<body>
<?= MenuWidget::widget()?>
<div class="container">
    <?=$content?>
</div>
</body>
</html>