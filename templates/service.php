<div class="title">
    <h1>Служба доставки "<?= $service_name?>"</h1>
</div>

<?php if (! empty($error)) :?>
<div class="alert alert-danger error"><?= $error ?></div>
<?php endif ?>

<form class="search" method="post">
    <div class="form-group">
        <label>Чтобы узнать стоимость товара, введите название или код товара и выберите город доствки</label>
        <label for="name" class="sr-only">Название товара</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Название товара" value="<?= $name ?>">
    </div>
    <div class="form-group">
        <label for="code" class="sr-only">Код товара</label>
        <input type="text" class="form-control" id="code" name="code" placeholder="Код товара" value="<?= $code ?>">
    </div>
    <div class="form-group">
        <label for="code" class="sr-only"></label>
        <select class="form-control" id="kladr" name="kladr">
            <option value="0">Выберите город доставки</option>
            <?php
                $cities = [
                    1 => 'Амстердам',
                    2 => 'Кострома',
                    3 => 'Мадрид',
                    4 => 'Москва',
                    5 => 'Осло',
                ];
                for($i = 1; $i <= count($cities); $i++): ?>
                    <option value="<?= $i ?>"<?php if($i == $kladr): ?> selected<?php endif ?>>
                        <?= $cities[$i] ?>
                    </option>
            <?php endfor ?>
        </select>
    </div>
    <input type="submit" name="submit" class="btn btn-info" value="Узнать стоимость">
</form>
<table class="table table-striped table-hover table-condensed table-bordered">
    <thead>
    <tr>
        <th class="text-center">#</th>
        <th>Товар</th>
        <th>Код</th>
        <th>Доставка</th>
    </tr>
    </thead>
    <tbody>

    <?php if (count($result) > 0): ?>
        <?php foreach($result as $key => $item): ?>
            <?php if (empty($item['data']['error'])): ?>
            <tr class="success">
                <td class="text-center"><?= ++$key?></td>
                <td><?= $item['goods']->getName()?></td>
                <td><?= $item['goods']->getCode()?></td>
                <td>
                    <div class="col-lg-6">
                        Стоимость: <?= $item['data']['price'] ?> руб.
                    </div>
                    <div class="col-lg-6">
                        Дата доставки: <?= date('d.m.Y', $item['data']['date']) ?>
                    </div>
                </td>
            </tr>
            <?php else: ?>
                <tr class="danger">
                    <td class="text-center"><?= ++$key?></td>
                    <td><?= $item['goods']->getName()?></td>
                    <td><?= $item['goods']->getCode()?></td>
                    <td>
                        Ошибка: <?= $item['data']['error']?>
                    </td>
                </tr>
            <?php endif ?>
        <?php endforeach ?>
    <?php else: ?>
        <tr><td colspan="4">Список товаров пуст</td></tr>
    <?php endif ?>
    </tbody>
</table>