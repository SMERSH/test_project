<?php

namespace models;

class Goods
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name,$code;

    public function __construct(int $id, string $name, string $code)
    {
        $this->id = $id;
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    public static function create(array $data)
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['code']
        );
    }
}
