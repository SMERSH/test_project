<?php

namespace models;

class GoodsMapper extends AbstractMapper
{
    /**
     * @var string название хранилища
     */
    public $storage = 'goods';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Метод для создания объекта с переданными параметрами
     * @param array $row
     * @return mixed
     */
    protected function rowToObject(array $row)
    {
        return Goods::create($row);
    }
}
