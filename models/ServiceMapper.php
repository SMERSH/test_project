<?php

namespace models;

class ServiceMapper extends AbstractMapper
{
    /**
     * @var string название хранилища
     */
    public $storage = 'service';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Метод для создания объекта с переданными параметрами
     * @param array $row
     * @return mixed
     */
    protected function rowToObject(array $row)
    {
        return Service::create($row);
    }
}
