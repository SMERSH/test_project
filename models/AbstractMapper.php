<?php

namespace models;

use configs\Config;
use drivers\iDriver;
use Exception;
use InvalidArgumentException;

abstract class AbstractMapper implements iDriver
{
    /**
     * @var string драйвер, использующийся для работы с записями
     */
    public $driver;

    public function __construct()
    {
        if(is_null($this->driver)){
            if (is_file(realpath(dirname(dirname(__FILE__))) . '/drivers/' . Config::$driver_name . '.php')) {
                $driver_name = 'drivers\\' . Config::$driver_name;
                //создаем объект драйвера
                $this->driver = new $driver_name(Config::$driver_params[Config::$driver_name][$this->storage]);
            } else {
                //бросаем исключение
                throw new Exception('Can\'t find file with name "' . Config::$driver_name . '"');
            }
        }
    }

    /**
     * Метод получения всех данных
     *
     * @return array $data
     */
    public function getAll()
    {
        $data = [];
        foreach($this->driver->getAll() as $row){
            $data[] = $this->rowToObject($row);
        }

        return $data;
    }

    /**
     * Метод, служит для поиска объекта по id
     *
     * @param int $id
     * @throws \InvalidArgumentException
     * @return array
     */
    public function findById($id)
    {
        $result = $this->driver->findById($id);

        if ($result === null) {
            throw new InvalidArgumentException('Object #' . $id . ' not found');
        }

        return $this->rowToObject($result);
    }

    /**
     * Метод, служит для поиска необходимых записей по переданным параметрам
     *
     * @param array $params
     * @return array
     */
    public function findByParams(array $params)
    {
        $data = [];
        foreach($this->driver->findByParams($params) as $row){
            $data[] = $this->rowToObject($row);
        }

        return $data;
    }

    /**
     * Метод для создания объекта с переданными параметрами
     * @param array $row
     * @return mixed
     */
    abstract protected function rowToObject(array $row);
}
