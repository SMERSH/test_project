<?php

namespace models;

class Service
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name, $className, $config;

    public function __construct(int $id, string $name, string $className, $config = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->className = $className;
        $this->config = $config;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string JSON
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return array
     */
    public function getConfigToArray()
    {
        return json_decode($this->getConfig(), true);
    }

    public static function create(array $data)
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['className'],
            $data['config']
        );
    }

    /**
     * Метод, возвращает данные о доставке(стоимость и дату)
     * @param $kladr
     * @param $goods
     * @return array
     */
    public function getDeliveryData($kladr, $goods)
    {
        $class_name = 'components\\' . ucfirst($this->className) . 'Service';

        //создаем объект api службы доставки
        $api = new $class_name($kladr, $goods, $this->getConfigToArray());

        return $api->getData();
    }
}
