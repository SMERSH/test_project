<?php

namespace widgets\Menu;

use widgets\Widget;
use models\ServiceMapper;

class MenuWidget extends Widget
{
    function run()
    {
        $serviceMapper = new ServiceMapper();

        return $this->renderPartial('../widgets/Menu/template/navbar',[
            'services' => $serviceMapper->getAll(),
        ]);
    }
}
