<?php

namespace core;

/**
 * Базовый класс контроллера
 */
abstract class SimpleController
{
    /**
     * @var View
     * Объект, который может использоваться для визуализации представлений или файлов просмотра
     */
    protected $view;

    function __construct(){
        $this->view = new View();
    }

    /**
     * Статический метод, позволяет достать из массива $_REQUEST необходимый элемент
     *
     * @param string $param_name
     * @return string|null
     */
    public static function getParam($param_name)
    {
        //проверяем на существование в массиве запрашиваемой переменной
        if (isset($_REQUEST[$param_name])) {
            return $_REQUEST[$param_name];
        }

        return null;
    }

    /**
     * Метод, запускающий необходимый экшен
     *
     * @param $action
     * @return string|null
     */
    protected function runAction($action)
    {
        //проверяем на существование в объекте необходимого экшена
        if(method_exists($this, 'action' . ucfirst($action))) {
            //запускаем на выполнение необходимый экшен
            return call_user_func(array($this, 'action' . ucfirst($action)));
        }

        return null;
    }

    /**
     * Абстрактный метод, позволяющий запускать дефолтный экшен
     */
    protected abstract function actionDefault();
}
