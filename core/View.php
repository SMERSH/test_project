<?php

namespace core;

use configs\Config;

/**
 * View представляет объект представления в шаблоне MVC.
 */
class View
{
    /**
     * Возвращает отрендеренный шаблон
     * @param $template
     * @param array $params
     * @return string
     */
    protected function fetchPartial($template, $params = []){
        extract($params);
        ob_start();
        include Config::$path_to_templates . $template . '.php';

        return ob_get_clean();
    }

    /**
     * Выводит отрендеренный шаблон
     * @param $template
     * @param array $params
     */
    public function renderPartial($template, $params = []){
        echo $this->fetchPartial($template, $params);
    }

    /**
     * Возвращает отрендеренный шаблон в layout'е
     * @param $template
     * @param array $params
     * @return string
     */
    private function fetch($template, $params = []){
        $content = $this->fetchPartial($template, $params);

        return $this->fetchPartial('layout', ['content' => $content]);
    }

    /**
     * Выводит отрендеренный шаблон в layout'е
     * @param $template
     * @param array $params
     */
    public function render($template, $params = []){
        echo $this->fetch($template, $params);
    }
}
