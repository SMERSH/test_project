<?php

namespace core;

use configs\Config;

class Router
{
    private $uri;

    public function getUri()
    {
        if (is_null($this->uri)) {
            $this->uri = ($_SERVER['REQUEST_URI'] == '/') ? Config::$default_url : $_SERVER['REQUEST_URI'];
        }

        return $this->uri;
    }

    public function run()
    {
        $url_parameters  = explode('/', trim($this->getUri(), '/'));

        // Первый параментр — контроллер.
        $controller_name = ucfirst(array_shift($url_parameters)) . 'Controller';

        // Второй — действие.
        $action = array_shift($url_parameters);
        $action = (is_null($action)) ? 'default' : $action;
        $action = 'action' . ucfirst($action);

        $controller = 'controllers\\' . ucfirst($controller_name);

        if (is_callable([$controller, $action])) {
            call_user_func_array([new $controller(), $action], $url_parameters);
        }else{
            die ('404 not found');
        }
    }
}