<?php

namespace core;

/**
 * Приложение является базовым классом для веб-приложений.
 */
class Application
{
    public function run()
    {
        $router = new Router();
        $router->run();
    }
}
