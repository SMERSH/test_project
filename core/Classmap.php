<?php
/**
 * Массив пространства имен
 */
$baseDir = dirname(__DIR__);

return [
    'core' => __DIR__,
    'configs' => $baseDir . DIRECTORY_SEPARATOR . 'configs',
    'controllers' => $baseDir . DIRECTORY_SEPARATOR . 'controllers',
    'drivers' => $baseDir . DIRECTORY_SEPARATOR . 'drivers',
    'models' => $baseDir . DIRECTORY_SEPARATOR . 'models',
    'components' => $baseDir . DIRECTORY_SEPARATOR . 'components',
    'widgets' => $baseDir . DIRECTORY_SEPARATOR . 'widgets',
];
