<?php
    //устанавливаем отображение всех ошибок (для более простого отлаживания приложения)
    ini_set('display_errors', true);
    error_reporting(E_ALL);

    require_once __DIR__ . '/core/Autoloader.php';
    (new core\Application)->run();
