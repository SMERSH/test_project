<?php

namespace controllers;

use core\SimpleController;
use models\GoodsMapper;
use models\ServiceMapper;

class DeliveryController extends SimpleController
{
    /**
     * Метод вызывает дефолтный экшн
     */
    public function actionDefault()
    {
        $this->runAction('listGoods');
    }

    /**
     * Вывод списока товаров
     * @return string
     */
    public function actionListGoods()
    {
        $goodsMapper   = new GoodsMapper();
        $serviceMapper = new ServiceMapper();

        $params = [
            'name' => $this->getParam('name'),
            'code' => $this->getParam('code')
        ];

        return $this->view->render('listGoods',[
            'goods'    => $goodsMapper->findByParams($params),
            'services' => $serviceMapper->getAll(),
            'params'   => $params
        ]);
    }

    /**
     * Ajax экшн возвращает стоимость и дату доставки у выбраной службы
     * @return string JSON
     */
    public function actionGetDeliveryData()
    {
        header('Content-type: application/json');

        $service_id = $this->getParam('service_id');
        $kladr      = $this->getParam('kladr');
        $goods_id   = $this->getParam('goods_id');

        $goodsMapper   = new GoodsMapper();
        $serviceMapper = new ServiceMapper();

        $goods   = $goodsMapper->findById($goods_id);
        $service = $serviceMapper->findById($service_id);

        echo json_encode($service->getDeliveryData($kladr, $goods));
    }

    /**
     * Поиск продукта, вычисление стоимости
     * и даты доставки у конкретной службы
     * @param $id
     * @return string
     */
    public function actionService($id)
    {
        $serviceMapper = new ServiceMapper();
        $service       = $serviceMapper->findById($id);

        $name  = $this->getParam('name');
        $code  = $this->getParam('code');
        $kladr = $this->getParam('kladr');

        // Сообщение об ошибке, если не заполнены поля
        $error = '';
        // массив содержит результат поиска
        $result = [];
        if (! is_null($this->getParam('submit'))) {
            if (!$name && !$code) {
                $error = 'Заполните хотя бы одно поле "Навание товара" или "Код товара" ';
            } elseif ($kladr == 0) {
                $error = 'Выберите город доставки';
            } else {
                $goodsMapper = new GoodsMapper();
                $goods       = $goodsMapper->findByParams([
                    'name' => $name,
                    'code' => $code
                ]);

                if($goods){
                    // перебираем найденные товары и узнаем стоимость доставки
                    foreach($goods as $item){
                        $result[] = [
                            'goods' => $item,
                            'data' => $service->getDeliveryData($kladr, $item)
                        ];
                    }
                } else {
                    $error = 'Товар не найден';
                }
            }
        }

        return $this->view->render('service', [
            'service_name' => $service->getName(),
            'error'        => $error,
            'name'         => $name,
            'code'         => $code,
            'kladr'        => $kladr,
            'result'       => $result
        ]);
    }
}
