<?php

namespace drivers;

class ArrayDriver extends AbstractDriver implements iDriver
{
    /**
     * @var array массив данных
     */
    public $data = [];

    public function __construct($storage)
    {
        $this->data = $storage;
    }

    /**
     * Метод получения всех данных
     *
     * @return array $data
     */
    public function getAll()
    {
        return $this->data;
    }

    /**
     * Метод, служит для поиска объекта по id
     *
     * @param int $id
     * @throws \InvalidArgumentException
     * @return array
     */
    public function findById($id)
    {
        $result = array_filter($this->data, function ($item) use ($id){
            return $item["id"]==$id;
        });

        if (count($result) > 0) {
            return array_shift($result);
        }

        return null;
    }

    /**
     * Метод, служит для поиска необходимых записей по переданным параметрам
     *
     * @param array $params
     * @return array
     */
    public function findByParams(array $params)
    {
        $conditions = [];
        foreach ($params as $field => $value) {
            if (! empty($value)) {
                $conditions[$field] = $value;
            }
        }

        $result = array_filter($this->data, function ($item) use ($conditions){
            return count(array_uintersect_assoc($item, $conditions,function($val, $condition){
                return (mb_strtolower($val) == mb_strtolower($condition)) ? 0 : 1;
            })) == count($conditions);
        });

        return $result;
    }
}
