<?php

namespace drivers;

/**
 * Интерфейс, описывающий необходимые драйверам методы
 */
interface iDriver
{
    /**
     * Метод получения всех данных
     *
     * @return array $data
     */
    public function getAll();

    /**
     * Метод, служит для поиска объекта по id
     *
     * @param int $id
     * @return array
     */
    public function findById($id);

    /**
     * Метод, служит для поиска необходимых записей по переданным параметрам
     *
     * @param array $params
     * @return array
     */
    public function findByParams(array $params);
}
